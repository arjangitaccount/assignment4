﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//can only be placed on camera
[RequireComponent(typeof(Camera))]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    private int halfer = 0;
    public int fraction = 5;

    // Update is called once per frame
    void Update()
    {


        {
            //if we click left in the mouse input controller
            if (Input.GetButton("Fire1"))
            {
                halfer++;
                if (halfer == fraction)
                {
                    //gets where you click in world space
                    Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition + Vector3.forward);

                    //aims the recently created balls in this direction
                    Vector3 FireDirection = clickPoint - this.transform.position;

                    //resets the direction of the vector3 to a magnitude (direction) of 1
                    FireDirection.Normalize();

                    //creation of ball
                    GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);

                    //sets the speed
                    prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;

                    print("create");
                    halfer = 0;
                }
            }
        }

    }
}
