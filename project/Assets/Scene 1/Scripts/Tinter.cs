﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{

 

    // Use this for initialization
    void Start()
    {
        
        //sets a random color for each attached item
        GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );


    }

    void Update()
    {

    }

}
