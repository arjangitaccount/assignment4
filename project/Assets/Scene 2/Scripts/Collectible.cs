﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    
    public float Radius;

    public void Update()
    {
        //test if the player is close to the orb
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );

        //if so
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            //if we run into the player
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                //destroys the orb
                Destroy( this.gameObject );
            }
        }
    }

}
