﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{

    private int reducer;
    public int fraction_optimized;

	// Update is called once per frame
	void Update ()
    {
        reducer++;
        if (reducer == fraction_optimized)
        {
            //gameobject.find is a very professional way to reference objects in scene
            Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();

            //define a raycast
            RaycastHit hit = new RaycastHit();

            //gets the location where the raycast hits the ground
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                //moves the character with navmesh to point
                GetComponent<NavMeshAgent>().destination = hit.point;
            }
            reducer = 0;
        }
	}
}
